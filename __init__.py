bl_info = {
	"name": "Arbol Object",
	"author": "Jon Mihkkal Inga, Michel Anders (varkenvarken)",
	"version": (1, 0),
	"blender": (2, 7, 9),
	"location": "View3D > Add > Mesh > New Arbol",
	"description": "Adds a bunch of trees",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Add Mesh"}

from math import radians
from random import random, seed

import bpy
from bpy.types import Operator
from bpy.props import FloatProperty,				\
    FloatVectorProperty,		\
    IntProperty,				\
    StringProperty,				\
    BoolProperty,				\
	EnumProperty
from mathutils import Vector, Matrix

from .lsystem import Turtle, Edge, Quad, BObject, Meta


def nupdate(self, context):

	for n in range(self.nproductions):
		namep = 'prod' + str(n+1)
		namem = 'mod' + str(n+1)
		nameq = 'prob' + str(n+1)

		try:
			s = getattr(self, namep)
		except AttributeError:
			setattr(self.__class__, namep,
                            StringProperty(
                                name=namep,
                                description="replacement string")
           )
		try:
			s = getattr(self, namem)
		except AttributeError:
			setattr(self.__class__, namem,
                            StringProperty(
                                name=str(n+1),
                                description="a single character module, but mighty fine",
                                maxlen=1)
           )
		try:
			s = getattr(self, nameq)
		except AttributeError:
			setattr(self.__class__, nameq, 
							FloatProperty(name='prob' + str(n+1), 
							min=0.0, 
							max=1.0, 
							default=1.0)
			)

class OBJECT_OT_add_arbol(Operator):
	"""Add a bunch of trees Object"""
	bl_idname = "mesh.add_arbol"
	bl_label = "Arbol"
	bl_description = "Grow me a forest"
	bl_options = {'REGISTER', 'UNDO', 'PRESET'}

	nproductions = IntProperty(
		name="productions",
		min=0,
		max=50,
		update=nupdate)

	niterations = IntProperty(
		name="iterations",
		min=0,
		max=15)

	seed = IntProperty(name="seed")

	start = StringProperty(
            name='start',
            default="F"
        )

	angle = FloatProperty(
		name='angle',
		default=radians(30),
		subtype='ANGLE',
		description="size in degrees of angle operators")

	tropism = FloatVectorProperty(
		name='tropism',
		subtype='DIRECTION',
		default=(0.0, 0.0, -1.0),
		description="direction of tropism")

	tropismsize = FloatProperty(
		name='tropism size',
		description="size of tropism")

	smooth_operator = BoolProperty(
            name='smooth_operator',
            default=False
        )

	canopy_scaling = BoolProperty(
            name='canopy_scaling',
            default=True,
			description="If selected, node size will effect scaling"
        )

	canopy_shape = EnumProperty(
			name="canopy_shape",
			description="Falloff mode for canopy objects",
			items=[
				('NONE', 'All the same', '', 'GHOST_DISABLED', 1),
				('SPHERICAL', 'Spherical', '', 'MESH_UVSPHERE', 2),
				('CONICAL', 'Conical', '', 'MESH_CONE', 3)
			],
			default='NONE'
		)

	canopy_height = FloatProperty(
		name="canopy_height",
		default=5.0
	)

	canopy_radius = FloatProperty(
		name="canopy_radius",
		default=2.5
	)


	meta_type = EnumProperty(
		name="meta_type",
		description="Yes",
		items=[
			('CUBE', 'Cube', '', 'META_CUBE', 1),
			('BALL', 'Ball', '', 'META_BALL', 2)
		],
		default='BALL'
	)

	meta_resolution = FloatProperty(
		name='meta_resolution',
		default=0.2,
		description="Resolution of metaballs")
	
	meta_radius = FloatProperty(
		name='meta_radius',
		default=10.0,
		description="relative radius of metaballs")

	ngenerations = IntProperty(
            name='ngenerations',
            description="number of generations",
            min=1,
            max=10,
            default=1
        )

	nspecimen = IntProperty(
            name='nspecimen',
            description="number of specimen",
            min=1,
            max=10,
            default=1
        )

	gridstep = FloatProperty(
            name='gridstep',
            description="distance between trees",
            min=0.0,
            default=1.0,
            subtype='DISTANCE'
        )

	def draw(self, context):
		layout = self.layout

		box = layout.box()
		box.prop(self, 'nproductions')
		box = layout.box()
		if getattr(self, 'start') == '':
			box.alert = True
		box.prop(self, 'start')

		for i in range(self.nproductions):
			namep = 'prod' + str(i+1)
			namem = 'mod' + str(i+1)
			nameq = 'prob' + str(i+1)

			box = layout.box()
			row = box.row(align=True)
			if getattr(self, namem) == '' or \
				getattr(self, namep) == '':
					row.alert = True
			row.prop(self, namem)
			row.prop(self, namep, text="")
			row.prop(self, nameq, text="frac")

		box = layout.box()
		box.label(text="Interpretation section")
		box.prop(self, 'niterations')
		box.prop(self, 'seed')
		box.prop(self, 'angle')
		box.prop(self, 'tropism')
		box.prop(self, 'tropismsize')
		
		box = layout.box()
		box.label(text="Fun with geometry")
		box.prop(self, 'meta_type')
		box.prop(self, 'meta_radius')
		box.prop(self, 'meta_resolution')
		box.prop(self, 'smooth_operator')
		
		box.label(text="Canopy")
		box.prop(self,'canopy_shape')
		box.prop(self,'canopy_scaling')
		box.prop(self,'canopy_height')
		box.prop(self,'canopy_radius')
		

		box = layout.box()
		box.label(text="Family tree")
		box.prop(self, 'ngenerations')
		box.prop(self, 'nspecimen')
		box.prop(self, 'gridstep')

	def iterate(self):
		class ldict(dict):
			def __missing__(self, key):
				return key, 2.0
		
		seed(self.seed)
		s = self.start

		prod = ldict()
		for i in range(self.nproductions):
			namep = 'prod' + str(i+1)
			namem = 'mod' + str(i+1)
			nameq = 'prob' + str(i+1)
			prod[getattr(self, namem)] = getattr(self, namep), getattr(self, nameq)
		for i in range(self.niterations):
			si = ""
			for c in s:
				if random() < prod[c][1]:
					si += prod[c][0]
			s = si
		return s

	@staticmethod
	def add_obj(obdata, context):
		scene = context.scene
		obj_new = bpy.data.objects.new(obdata.name, obdata)
		base = scene.objects.link(obj_new)
		return obj_new, base

	@staticmethod
	def add_obj_cpy(original, context):
		scene = context.scene
		obj_new = original.copy()
		obj_new.data = original.data.copy()
		base = scene.objects.link(obj_new)
		return obj_new, base

	def canopy_scale(self, pos):
		if self.canopy_shape is 'SPHERICAL':
			return 1.0
		
		else:
			return 1.0




	def interpret(self, s, context):
		q = None
		qv = ((0.5, 0, 0), (0.5, 1, 0), (-0.5, 1, 0), (-0.5, 0, 0))
		verts = []
		edges = []
		quads = []
		metas = []
		self.radii = []

		t = Turtle(	self.tropism,
                    self.tropismsize,
                    self.angle,
                    self.seed)

		canopyMaterial = bpy.data.materials.get("Canopy")
		if canopyMaterial is None:
			canopyMaterial = bpy.data.materials.new(name="Canopy")
		

		trunkMaterial = bpy.data.materials.get("Trunk")
		if trunkMaterial is None:
			trunkMaterial = bpy.data.materials.new(name="Trunk")
		

		
		for e in t.interpret(s):
			if isinstance(e, Edge):
				si, ei = (	verts.index(v)
                                    if v in verts
                                    else (	len(verts),
                                           verts.append(v),
                                           self.radii.append(e.radius))[0]
                                    for v in (e.start, e.end))
				edges.append((si, ei))
			elif isinstance(e, Quad):
				if q is None:
					q = bpy.data.meshes.new('lsystem-leaf')
					q.from_pydata(qv, [], [(0, 1, 2, 3)])
					q.update()
					q.uv_textures.new()
				obj, base = self.add_obj(q, context)
				r = Matrix()
				for i in (0, 1, 2):
					r[i][0] = e.right[i]
					r[i][1] = e.up[i]
					r[i][2] = e.forward[i]
				obj.matrix_world = Matrix.Translation(e.pos)*r
				quads.append(obj)

			elif isinstance(e, BObject):
				if bpy.data.objects.get(e.name) is not None:
					original = bpy.data.objects[e.name]
				elif bpy.data.objects.get('Cube') is not None:
					original = bpy.data.objects['Cube']
				else:
					bpy.ops.mesh.primitive_cube_add()
					original = bpy.data.objects['Cube']

				obj, base = self.add_obj_cpy(original, context)
				r = Matrix()
				for i in (0, 1, 2):
					r[i][0] = e.right[i]
					r[i][1] = e.up[i]
					r[i][2] = e.forward[i]
				obj.matrix_world = Matrix.Translation(e.pos)*r
				quads.append(obj)

			elif isinstance(e, Meta):
				metas.append(e)
		
		mesh = bpy.data.meshes.new('arbol')
		mesh.from_pydata(verts, edges, [])
		mesh.update()
		obj, base = self.add_obj(mesh, context)
		
		if len(metas) > 0:
			mball = bpy.data.metaballs.new('MetaBall')
			mball.resolution = self.meta_resolution
			o = bpy.data.objects.new('Meta_Obj', mball)
			context.scene.objects.link(o)
			
			for e in metas:
				this_ball = mball.elements.new()
				this_ball.type = self.meta_type
				this_ball.use_negative = False
				this_ball.radius = self.canopy_scale(e.pos) * e.radius * self.meta_radius
				this_ball.co = e.pos + e.forward*this_ball.radius/2
			
			bpy.context.scene.update()
			m = o.to_mesh(context.scene, False, 'PREVIEW')

			can_obj = bpy.data.objects.new('Canopy', m)

			if can_obj.data.materials:
				can_obj.data.materials[0] = canopyMaterial			
			else:
				can_obj.data.materials.append(canopyMaterial)

			for p in can_obj.data.polygons:
				p.use_smooth = False

			context.scene.objects.link(can_obj)
			context.scene.objects.unlink(o)
			can_obj.parent = obj
			
		
		if obj.data.materials:
			obj.data.materials[0] = trunkMaterial
		else:
			obj.data.materials.append(trunkMaterial)	

		if len(metas) > 0:
			obj.data.materials.append(canopyMaterial)


		for ob in context.scene.objects:
			ob.select = False
		base.select = True
		context.scene.objects.active = obj
		for q in quads:
			q.parent = obj
		return base

	def execute(self, context):

		start_gen = self.niterations
		start_seed = self.seed

		for m in range(0, self.nspecimen):
			self.seed = start_seed + m
			for n in range(0, self.ngenerations):
				self.niterations = start_gen + n
				s = self.iterate()
				obj = self.interpret(s, context)

				bpy.ops.object.modifier_add(type='SKIN')
				context.active_object.modifiers[0].use_smooth_shade = False
				context.active_object.modifiers[0].use_x_symmetry = False

				skinverts = context.active_object.data.skin_vertices[0].data

				for i, v in enumerate(skinverts):
					v.radius = [self.radii[i], self.radii[i]]

				if self.smooth_operator:
					bpy.ops.object.modifier_add(type='SUBSURF')
					context.active_object.modifiers[1].levels = 2

				context.active_object.location = (m*self.gridstep, n*self.gridstep, 0.0)

		self.niterations = start_gen
		self.seed = start_seed
		return {'FINISHED'}


def add_object_button(self, context):
	self.layout.operator(
		OBJECT_OT_add_arbol.bl_idname,
		text="Arbol!",
		icon='PLUGIN')


def register():
	bpy.types.INFO_MT_mesh_add.remove(add_object_button)
	bpy.utils.register_class(OBJECT_OT_add_arbol)
	bpy.types.INFO_MT_mesh_add.append(add_object_button)


def unregister():
	bpy.utils.unregister_class(OBJECT_OT_add_arbol)
	bpy.types.INFO_MT_mesh_add.remove(add_object_button)


if __name__ == "__main__":
	register()
